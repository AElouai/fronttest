import { Component, OnInit , Output , EventEmitter } from '@angular/core';

@Component({
  selector: 'app-right-side',
  templateUrl: './right-side.component.html',
  styleUrls: ['./right-side.component.css' , '../feed/feed.component.css']
})
export class RightSideComponent implements OnInit {

  vimeoDef = {
    page: 1,
    per_page: 50,
    sort: 'likes',
    direction: 'desc',
    query : 'music'
  };
  @Output() options: EventEmitter<any> =  new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
    this.getPosts();
  }

  getPosts() {
    this.options.emit( this.vimeoDef );
  }
}
