import { Component, OnInit , ChangeDetectorRef } from '@angular/core';
import { VimeoService } from '../services/vimeo.service';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {

  posts;
  rPosts;
  lib;
  post_per_page = 10;
  options;

/*  options = {
    searchQuery : '',
    minLike : 0
  };*/
/*  vimeoDef = {
    page: 1,
    per_page: 50,
    sort: 'likes',
    direction: 'desc',
    query : 'music'
  };*/

  constructor(private vimeoService: VimeoService , private ref: ChangeDetectorRef ) { }

  ngOnInit() {
  }

  getPosts (dataOptions) {
    /**
     * Storing data into rPosts to preserve raw data as it come form server
     * and using posts to put only paginated data
     */
    this.vimeoService.getPosts( dataOptions )
      .map((response) => response)
      .subscribe( data => {
        this.rPosts = data ;
        this.posts  = this.rPosts.slice(0 , this.post_per_page );
        this.update();
      });
  }


  /**
   * Open URL link into new window
   * @param link : string
   */
/*  goTo( link: string ) {
    window.open(link);
  }*/

  /**
   * Videos pagination
   */
  nextPostsSet() {
    /**
     * using the real raw data
     */
    if ( this.post_per_page === 10 ) {
      this.post_per_page = 25;
      this.posts  = this.rPosts.slice(0 , 25);
    }else if ( this.post_per_page === 25 ) {
      this.post_per_page = 50;
      this.posts  = this.rPosts.slice(0 , 50 );
    }
  }

  update() {
    /**
     * communique the last changes to the DOM
     */
    this.ref.detectChanges();
  }

  /**
   * Setting filtering data form leftSide component
   * @param data
   */
  optionsChange(data) {
    this.options = data;
  }

}
