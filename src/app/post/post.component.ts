import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css' , '../feed/feed.component.css']
})
export class PostComponent implements OnInit {

  @Input() post: any;

  constructor() { }

  ngOnInit() {
  }


  /**
   * Open URL link into new window
   * @param link : string
   */
  goTo( link: string ) {
    window.open(link);
  }
}
