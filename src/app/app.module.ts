import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FeedComponent } from './feed/feed.component';
import { VimeoService } from './services/vimeo.service';
import { LimitTextPipe } from './Pipe/limit-text.pipe';
import { NumberSuffixPipe } from './Pipe/number-suffix.pipe';
import { SearchPipe } from './Pipe/search.pipe';
import { PostComponent } from './post/post.component';
import { LeftSideComponent } from './left-side/left-side.component';
import { RightSideComponent } from './right-side/right-side.component';

@NgModule({
  declarations: [
    AppComponent,
    FeedComponent,
    LimitTextPipe,
    NumberSuffixPipe,
    SearchPipe,
    PostComponent,
    LeftSideComponent,
    RightSideComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [ VimeoService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
