import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberSuffix'
})
/**
 * Transform number to String with suffix
 * 12350 = 12.5K
 */
export class NumberSuffixPipe implements PipeTransform {

  transform(value: number, args?: number): any {
    if ( value < 1000) {
      return value;
    } else {
      /*
      Representation of number thousand million ..
       */
      let suffixes = ['k', 'M', 'G', 'T', 'P', 'E'];
      let exp = Math.floor(Math.log(value) / Math.log(1000));
      return (value / Math.pow(1000, exp)).toFixed(args) + suffixes[exp - 1];

    }
  }

}
