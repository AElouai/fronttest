import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'limitText'
})
/**
 * Limit the size of text to display
 */
export class LimitTextPipe implements PipeTransform {

  transform(value: string, args: string): string {
    let limit = args ? parseInt(args, 10) : 150;
    let trail = ' ...';
    return value != null ? value.length > limit ? value.substring(0, limit) + trail : value : '';
  }

}
