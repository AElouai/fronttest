import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
/**
 * Filtering list based on query string and number of likes
 */
export class SearchPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    // In case we haven't any data yet
    if ( value === undefined || value === null){
      return value;
    } else {
      // filter array
      return value.filter(item => {
        if ( ( '' + item.description.toLowerCase() ).includes(args.searchQuery.toLowerCase())
          && item.metadata.connections.likes.total >= args.minLike) {
          return true;
        }
        if ( ( '' + item.name.toLowerCase() ).includes(args.searchQuery.toLowerCase())
          && item.metadata.connections.likes.total >= args.minLike) {
          return true;
        }
        return false;
      });
    }
  }
}
