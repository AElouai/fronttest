import { Injectable } from '@angular/core';
import { Vimeo } from 'vimeo';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class VimeoService {

  /**
   * Todo Creating a service to get CLIENT_SECRET form a server API or abstract it a least
   * Develapment Mode Only
   */
  CLIENT_ID = '2afc689a5a199909ecbf813ea6b70e1def7c3c05' ;
  CLIENT_SECRET = 'MCwll5aQQKlqyhGoh2/4QWKfiZNCoTVOLJKq/PNv3/3W/iE7ZVaGWFmSnduA+MIYvTkrq+yjs6vT+NSLVMCQaobULJxodmLreFMzInu0Czf5xgJuoZ/ijAEr3bHYFppR';
  ACCESS_TOKEN = 'f59f2b309f6ddb897f8e9eb86df16711';
  SCOPES = 'public private purchased create edit delete interact upload';
  lib;

  constructor() {
    this.lib = new Vimeo(this.CLIENT_ID, this.CLIENT_SECRET, this.ACCESS_TOKEN);
  }

  /**
   * Get Post using Vimeo package
   * @param vimeoOptions  {
      page: number,
      per_page: number,
      sort: string,
      direction: string,
      query : string
    }
   */
  getPosts(vimeoOptions) {
    return new Observable(observer => {
      this.lib.request(/*options*/{
        // This is the path for the videos contained within the staff Top picks
        path : '/channels/top/videos',
        // This adds the parameters to request
        query : vimeoOptions
      }, /*callback options*/ (error, body ) => {
        if (error) {
          /**
           * Todo Handel Api error due to timeout or invalid token/data givin by client
           */
          observer.next([]);
        } else {
          /**
           * return row Data
           */
          observer.next(body.data);
        }
        observer.complete();
      });
    });

  }


}
