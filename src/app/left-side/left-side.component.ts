import { Component, OnInit , Output , EventEmitter } from '@angular/core';

@Component({
  selector: 'app-left-side',
  templateUrl: './left-side.component.html',
  styleUrls: ['./left-side.component.css' , '../feed/feed.component.css']
})
export class LeftSideComponent implements OnInit  {

  option = {
    searchQuery : '',
    minLike : 10,
    min : false
  };
  @Output() options: EventEmitter<any> =  new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
    this.changes();
  }

  changes() {
    this.options.emit( this.option );
  }

}
