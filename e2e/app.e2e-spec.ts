import { FrontTestPage } from './app.po';

describe('front-test App', () => {
  let page: FrontTestPage;

  beforeEach(() => {
    page = new FrontTestPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
